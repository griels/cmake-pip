from unittest import TestCase
import shutil
import tempfile
import os
import subprocess

use_sandboxed_dir = True


class TestCMake1(TestCase):

    def setUp(self):
        # installs the virtualenv
        import virtualenv

        if use_sandboxed_dir:
            sandbox_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, 'sandbox_test'))
            if not os.path.exists(sandbox_dir):
                os.makedirs(sandbox_dir)

            self.tempdir = tempfile.mkdtemp('__test', 'cmake_distutils',
                                            dir=sandbox_dir)
        else:
            self.tempdir = tempfile.mkdtemp('__test', 'cmake_distutils')

        virtualenv.create_environment(self.tempdir, site_packages=False, clear=True)

        # check for windows
        self.python_interpreter = os.path.join(self.tempdir, "bin", "python")

        # updates pip
        ret_code = subprocess.call([self.python_interpreter,
                                    '-m',
                                    'pip', 'install', '--upgrade', 'pip'])
        self.assertEqual(ret_code, 0)

        # installs itself into the venv
        ret_code = subprocess.call([self.python_interpreter,
                                    '-m',
                                    'pip', 'install', '--ignore-installed', '--no-cache-dir',  # '--no-download',
                                    os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))])
        self.assertEqual(ret_code, 0)

        # for debugging
        self.install_package('ipdb')

        subprocess.call([self.python_interpreter, '-m', 'pip', 'list'])
        self.check_package_installed('cmake-pip')
        pass

    def tearDown(self):
        # removes the virtualenv
        if not use_sandboxed_dir:
            shutil.rmtree(self.tempdir)
        pass

    def process_print_errors(self, process):

        if process.returncode != 0:
            print '#' * 100
            print 'LOGS'
            print process.stdout.readlines()
            print process.stderr.readlines()
            print '#' * 100

    def install_package(self, package_to_install, upgrade=False):
        l = [self.python_interpreter, '-m', 'pip', 'install']
        if upgrade:
            l += '--upgrade'
        l += [package_to_install]
        p = subprocess.Popen(l,
                             stdin=None,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        p.wait()
        self.assertEqual(p.returncode, 0)

    def check_package_installed(self, package_to_check):
        p = subprocess.Popen([self.python_interpreter, '-m', 'pip', 'list'],
                             stdin=None,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        out, _ = p.communicate()
        self.assertEqual(p.returncode, 0)

        for i in out.splitlines():
            if i.find(package_to_check) > -1:
                return

        raise RuntimeError('Package "%s" not found' % package_to_check)

    def test_simple_extension_with_cmake(self):
        """Generates a python extension with cmake, installs it and checks a command on the installed package"""
        # the package being installed is a very basic python extension.

        python_test_package = 'simple_ext_test1'

        ret_code = subprocess.call([self.python_interpreter,
                                    '-m',
                                    'pip', 'install', '--verbose', '--ignore-installed', '--no-clean',
                                    os.path.abspath(os.path.join(os.path.dirname(__file__), python_test_package))])
        self.assertEqual(ret_code, 0)

        self.check_package_installed('distutils-cmake-test1')

        # runs a test
        p = subprocess.Popen([self.python_interpreter, '-m', 'distutils_cmake_test1.check_extension'],
                             stdin=None,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        p.wait()

        self.process_print_errors(p)
        self.assertEqual(p.returncode, 0)
        pass

    def test_check_correct_python_interpreter(self):
        """Checks that the correct python interpreter is used during the build"""

        python_test_package = 'check_python_interpreter'

        # inspecting cmake cache is difficult because with pip the temporary folder gets deleted, even with the no clean
        # option
        p = subprocess.Popen([self.python_interpreter, '-m', 'pip', 'install', '--verbose', '--ignore-installed', '--no-clean',
                              os.path.abspath(os.path.join(os.path.dirname(__file__), python_test_package))],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        p.wait()

        self.assertNotEqual(p.returncode, 0)
        with self.assertRaises(RuntimeError):
            self.check_package_installed('distutils-cmake-print_python_interpreter')

        stdout = p.stdout.readlines()
        stderr = p.stderr.readlines()

        all_strings = [i.strip() for i in stderr + stdout if i.strip()]
        for i in all_strings:
            if "python found here".lower() in i.lower():
                ind = all_strings.index(i)
                s = ' '.join(all_strings[ind:(ind + 2)])
                print all_strings
                print s, self.python_interpreter
                self.assertGreaterEqual(s.find(self.python_interpreter), 0)
                break
        else:
            self.fail('String "python found here" not found')
        pass

    def test_configuration_failures_reported(self):
        """Checks that a failure in CMake is properly reported"""

        python_test_package = "failing_cmake_configuration"

        p = subprocess.Popen([self.python_interpreter, '-m', 'pip', 'install', '--verbose', '--ignore-installed', '--no-clean',
                              os.path.abspath(os.path.join(os.path.dirname(__file__), python_test_package))],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        p.wait()

        self.assertNotEqual(p.returncode, 0)

        with self.assertRaises(RuntimeError):
            self.check_package_installed('distutils-cmake-failing-config-test')

        stdout = p.stdout.readlines()
        stderr = p.stderr.readlines()

        self.assertIn("This cmake build should fail", [i.strip() for i in stderr + stdout])
        all_errors = [i.strip().lower() for i in stderr + stdout if "error" in i.strip().lower()]

        for i in all_errors:
            if "CMake Error".lower() in i:
                break
        else:
            self.fail('"CMake Error" not found')


# TODO
# source distribution creation and compilation/installation
# wheel distribution creation and installation
