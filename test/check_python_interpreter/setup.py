
from cmake_pip.cmake_extension import ExtensionCMake, setup, build_ext

# extension created by cmake
ext1 = ExtensionCMake('distutils-cmake-print_python_interpreter.cmake_dummy',
                      'cmake_fail_print_python_interpreter/CMakeLists.txt')

setup(name='distutils-cmake-print_python_interpreter',
      version='0.1a',
      packages=['distutils-cmake-print_python_interpreter'],
      package_dir={'distutils-cmake-print_python_interpreter': 'src'},
      ext_modules=[ext1],
      author='Raffi Enficiaud',
      author_email='raffi.enficiaud@free.fr',
      url='https://bitbucket.org/renficiaud/distutils-cmake',
      description='Fails by printing the python interpreter',
      license='Unlicensed',
      )
