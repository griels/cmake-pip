
# loads the dynamic library "cmake_test" and checks the result of a simple function


def test_extension():
    from .cmake_test import test
    assert(test() == 42)
    return test()

if __name__ == '__main__':
    print test_extension()
    import sys
    sys.exit(0)
