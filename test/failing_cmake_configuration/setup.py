
from cmake_pip.cmake_extension import ExtensionCMake, setup, build_ext

# extension created by cmake
ext1 = ExtensionCMake('distutils-cmake-failing-config-test.non_existing',
                      'cmake_fail/CMakeLists.txt')

setup(name='distutils-cmake-failing-config-test',
      version='0.1a',
      packages=['distutils-cmake-failing-config-test'],
      package_dir={'distutils-cmake-failing-config-test': 'src'},
      ext_modules=[ext1],
      author='Raffi Enficiaud',
      author_email='raffi.enficiaud@free.fr',
      url='https://bitbucket.org/renficiaud/distutils-cmake',
      description='this test fails at the configuration time',
      license='Unlicensed',
      )
