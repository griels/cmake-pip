#!python
# -*- coding: UTF-8 -*-

from setuptools import setup
from distutils.util import convert_path
from distutils import log
import os


classifiers = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'Operating System :: MacOS :: MacOS X',
    'Operating System :: Microsoft :: Windows :: Windows NT/2000',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: cmake',
    'Programming Language :: Python :: 2.7',
]

keywords = 'cmake', 'distutils', 'setuptools', 'pip', 'packaging', 'cross-platform'


def _get_version():
    """Convenient function returning the version of this package"""

    ns = {}
    version_path = convert_path('cmake_pip/version.py')
    if not os.path.exists(version_path):
        return None
    with open(version_path) as version_file:
        exec(version_file.read(), ns)

    log.warn('[VERSION] read version is %s', ns['__version__'])
    return ns['__version__']

setup(
    name='cmake_pip',
    version=_get_version(),

    author='Raffi Enficiaud',
    author_email='raffi.enficiaud@free.fr',

    url='https://bitbucket.org/renficiaud/cmake-pip',
    packages=['cmake_pip'],
    package_dir={'cmake_pip': 'cmake_pip'},
    package_data={'cmake_pip': ['cmake_modules/*.cmake']},
    classifiers=classifiers,
    keywords=keywords,
    license='Boost Software License - Version 1.0 - August 17th, 2003',
    description='distributing CMake projects with distutils/setuptools/pip',
    long_description=open('README.md').read(),
)
